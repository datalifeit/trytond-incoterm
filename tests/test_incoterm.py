# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class IncotermTestCase(ModuleTestCase):
    """Test Incoterm module"""
    module = 'incoterm'


del ModuleTestCase
