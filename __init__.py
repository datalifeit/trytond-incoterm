# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .sale import Sale, SaleIncoterm
from .stock import ShipmentOut, ShipmentOutIncoterm
from .invoice import Invoice, InvoiceIncoterm
from .incoterm import Rule, Version, RuleVersion
from .party import Party, PartyCustomerIncoterm


def register():
    Pool.register(
        Rule,
        Version,
        RuleVersion,
        Invoice,
        Sale,
        SaleIncoterm,
        ShipmentOut,
        ShipmentOutIncoterm,
        InvoiceIncoterm,
        Party,
        PartyCustomerIncoterm,
        module='incoterm', type_='model')
